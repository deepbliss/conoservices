<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'conoservices');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R{5:jIa9G@{}tjls yqCJR.]nfI3&f6nI^d=W)5[LNm/+cqqs.xb]6q_B~8QEOTU');
define('SECURE_AUTH_KEY',  'y,%b(lZ_Zg<YrXv#Y)652aG(+RKmoL;hmR|k%!}:P)K-K>$d8Ea1|Pot5eT]Ib55');
define('LOGGED_IN_KEY',    'WG~(d&{,d0psq`jmZCO00KaFRbf%;|~Z,Gcg%GqCi`gV7QwW6)=`uVnyS9coEXca');
define('NONCE_KEY',        'cE/Zor{G_LM;^1ymk dE<ji-wF6SC`|G71>]PKgaB}GP2mYQi}3l_Y{(%CV9)c,p');
define('AUTH_SALT',        '(vnNh63]|IezmQw,Q.jEeVqruk~opKDmf^9Lj/mYk<|pl%,@v_RtY8-V~F%~-csu');
define('SECURE_AUTH_SALT', '`4>g`@D3d>^!/RW{8|C?`_~k A.x[}zsH;TD2k{v[Iyg#BJnhc~P@ATm#uM$zcMD');
define('LOGGED_IN_SALT',   'e>znen*Frm)cV@e.eKiJZF}?YAcqP%Ryw8:iJSKdF=,BKjI8/59M@vf[cn_0yt( ');
define('NONCE_SALT',       ')](7 =|5=PS|F6BFap$4JG3fc@O=h1B|zZC>H++L}8e</$@!t.)o9`v>VVKe(Lg}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
