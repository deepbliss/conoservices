<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/ico" href="<?php the_field("favicon", 'option');?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">

<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
<![endif]-->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>  
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>
<script>
$(document).ready(function() {
$('#banner').owlCarousel({
loop: true,
items: 1,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: true,
dots: false

});
});
</script>
<script>
$(document).ready(function() {
$('#clients').owlCarousel({
loop: false,
items: 6,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: false,
dots: false,margin:17,
 responsiveClass:true,
 responsive:{
           0:{
               items:1
           },
           568:{
               items:2
               
           },
           640:{
               items:2
              
           },
           768:{
               items:3
           },
           1024:{
               items:4
               
           },
          
		   1600:{
               items:6
			   
           }
       },


});
});
</script>
<script>
$(document).ready(function() {
$('#project').owlCarousel({
loop: false,
items: 3,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: true,
dots: true,
margin:38,
 responsiveClass:true,
 responsive:{
           0:{
               items:1,
           },
           568:{
               items:1
               
           },
           640:{
               items:2
              
           },
           768:{
               items:2
           },
           1024:{
               items:3
               
           },
          
		   1600:{
               items:3
			   
           }
       },
      

});
});
</script>
<script type="text/javascript">
	$(document).ready(function() {
$('#project .owl-dot').each(function(){
$(this).children('span').text($(this).index()+1);
});
});
</script>

<script>
$(document).ready(function() {
$('#testimonial').owlCarousel({
loop: false,
items: 3,
autoplay: false,
smartSpeed: 1500,
rewindNav:false,
nav: false,
dots: false,margin:38,
 responsiveClass:true,
 responsive:{
           0:{
               items:1,
           },
           568:{
               items:1
               
           },
           640:{
               items:2
              
           },
           768:{
               items:2
           },
           1024:{
               items:3
               
           },
          
		   1600:{
               items:3
			   
           }
       },
      

});
});
</script>

<script>
$(function() {
$('nav#menu').mmenu({
extensions : [ 'effect-slide-menu', 'pageshadow' ],
searchfield : false,
counters : false,
navbar : {
title : 'Menu'
},
navbars : [
{
position : 'top',
<!--content : [ 'searchfield' ]-->               
}, {
position : 'top',
content : [
'prev',
'title',
'close'
]
}
]
});
});
</script>
 <script>
$window = $(window);
$window.scroll(function() {
  $scroll_position = $window.scrollTop();
    if ($scroll_position > 1) { // if body is scrolled down by 300 pixels
        $('header').addClass('sticky');

        // to get rid of jerk
        header_height = $('.header').innerHeight();
        $('body').css('padding-top' , header_height);
    } else {
        $('body').css('padding-top' , '0');
        $('header').removeClass('sticky');
    }
 });
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<header>
	<div class="header_top">
		<div class="container">
			<div class="header_top_rgt">
			<div class="header_call">
				<span>Phone: <?php the_field("header_call", 'option');?></span>
			</div>
			<div class="header_email"><span>Email: <a href="mailto:<?php the_field("header_email", 'option');?>"><?php the_field("header_email", 'option');?></a></span>
			</div>
			<div class="header_search">
				 <?php dynamic_sidebar( 'Search' ); ?>

			</div>
		</div>

 <nav id="menu">
                        
                          <?php wp_nav_menu( array('menu' => 'Header Menu')); ?>
                        
                    </nav>
</div>
	</div>
<div class="header_bottom">
	<div class="container">
		<div class="mob-nav">
                        <a class="menu-btn" href="#menu"></a>
                       
                    </div>
		<div class="logo">
			<a href="<?php echo site_url(); ?>"><img src="<?php the_field("logo", 'option');?>" alt=""></a>
		</div>
				<div class="desk-nav">

<?php wp_nav_menu( array('menu' => 'Header Menu')); ?>

</div>
		
	</div>
</div>

</header>

