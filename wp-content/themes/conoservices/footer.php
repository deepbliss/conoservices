<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

	<footer id="colophon" class="footer" role="contentinfo">
		<div class="footer_top">
			<div class="container">
			<div class="clmn_1">
				<div class="ft_logo">
					<a href="<?php echo site_url(); ?>"><img src="<?php the_field("footer_logo", 'option');?>" alt=""></a>
				</div>
				<div class="address">
					<?php the_field("footer_address", 'option');?>
				</div>
				<span class="footer_call"><?php the_field("footer_call", 'option');?></span>
				<span class="footer_email"><a href="mailto:<?php the_field("footer_email", 'option');?>"><?php the_field("footer_email", 'option');?></a></span>
			</div>
			<div class="clmn_2">
				<h4>Quick Links</h4>
				<?php wp_nav_menu( array('menu' => 'Footer Menu 1')); ?>
			</div>
			<div class="clmn_3">
				<h4>Information</h4>
				<?php wp_nav_menu( array('menu' => 'Footer Menu 2')); ?>
			</div>
			<div class="clmn_4">
				<h4>Here to Help</h4>
				<?php wp_nav_menu( array('menu' => 'Footer Menu 3')); ?>
			</div>
	</div>
</div>
<div class="footer_btm">
	<div class="container">
		<div class="social_icn">
				<h4>Get in Touch</h4>
				<ul>
					<li><a class="linkedin" href="<?php the_field("linkedin_link", 'option');?>" target="_blank" title="LinkedIn"></a></li>
					<li><a class="twitter" href="<?php the_field("twitter_link", 'option');?>" target="_blank" title="Twitter"></a></li>
				</ul>
			</div>
			<div class="btm_footer_logo">
				<a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo2.png"></a>
	</div>
	</div>
			
		</footer><!-- #colophon -->
	
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
