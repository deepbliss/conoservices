<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="home_inner">
 <div class="homepage-banner">
  <?php home_banner() ?>
   <div class="banner_btm">
 	<div class="container">
 	<h1><?php the_field("home_banner_text"); ?></h1>
 	<div class="banner_search">
 	<?php dynamic_sidebar( 'Search' ); ?>
 </div>
 <div class="services">
 <ul>
		<?php 
					if( have_rows('home_banner_services') ): 
						while ( have_rows('home_banner_services') ) : the_row();?>
						<li>
							<span>
								<figure>
									<img src="<?php the_sub_field("home_banner_image"); ?>">
								</figure>
							</span>
							<div class="banner_cnt">
								<h2><?php the_sub_field("home_banner_title"); ?></h2>
								<?php the_sub_field("home_banner_content"); ?>
								<a href="<?php the_sub_field("home_banner_link"); ?>">FIND OUT MORE</a>
						</div>
					
						</li>
					<?php endwhile; 
				endif; ?>
			</ul>
 </div>
</div>
</div>
 </div>
 <div class="home_middle_sec">
 	<div class="container">
 		<h3>Recent Projects</h3>
 <?php recent_project() ?>
</div>
</div>
<div class="partner_sec">
	<div class="container">
		<div class="heading">
				<h3><span>OUR PARTNERS</span></h3>
			</div>
				 <?php our_partner() ?>
			</div>
		</div>
		<div class="testimonial_sec">
			<div class="container">
			<div class="heading">
				<h3><span>WHAT OUR CLIENTS SAY ABOUT US</span></h3>
			</div>
			<?php testimonial() ?>
		</div>
		</div>
	</div>




<?php get_footer();
